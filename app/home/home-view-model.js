const axios = require("axios");
const observableModule = require("tns-core-modules/data/observable");
const SelectedPageService = require("../shared/selected-page-service");
// require("nativescript-nodeify");
const PubNub = require("pubnub");

const apiUrl = "https://dev.ios.skilleyes.com/api/";

// configure
const pubnubConfig = {
    publishKey: "pub-c-aef19144-b0cb-4a9e-8e38-bdb3f14ee7b5",
    subscribeKey: "sub-c-e036c610-00f0-11e8-8274-06cc4b902b08",
    uuid: "Umut Tufanoglu"
};

const accessToken = "";

function HomeViewModel() {
    SelectedPageService.getInstance().updateSelectedPage("Home");

    const viewModel = observableModule.fromObject({

        publish() {
            const pubnub = new PubNub(pubnubConfig);

            function publishSampleMessage() {
                console.log("Since we're publishing on subscribe connectEvent, we're sure we'll receive the following publish.");
                const publishConfig = {
                    channel: `${pubnubConfig.uuid}-stdby`,
                    message : {
                        title: "greeting",
                        description: "hello world!"
                    }
                };
                pubnub.publish(publishConfig, (status, response) => {
                    console.log(status, response);
                });
            }

            pubnub.addListener({
                status: (statusEvent) => {
                    if (statusEvent.category === "PNConnectedCategory") {
                        publishSampleMessage();
                    }
                },
                message: (msg) => {
                    console.log(msg.message.title);
                    console.log(msg.message.description);
                },
                presence: (presenceEvent) => {
                    // handle presence
                }
            });
            console.log("Subscribing..");
            pubnub.subscribe({
                channels: ["hello_world"]
            });
        },

        login(args) {
            const button = args.object;
            const data = {
                username: "ut@bluebox.de",
                password: "ut22992",
                registration_token: "umut_regis"
            };

            button.text = "Trying to login...";

            axios.post(`${apiUrl}login`, data)
                .then((response) => {
                    this.setAccessToken(response.data.access_token);
                    console.log(`access token: ${response.data.access_token}`);
                    console.log(response);
                    button.text = "Success";
                })
                .catch((error) => {
                    console.log(error);
                    button.text = "Error";
                });
        },

        sendInquiry(args) {
            const button = args.object;
            button.text = "Sending Inquiry...";

            const data = {
                client_id: 3,
                device_id: 1,
                inquiry_description: "Nativscript Description"
            };

            const headers = {
                headers: {
                    "Authorization": `Bearer ${this.getAccessToken()}`
                }
            };

            axios.post(`${apiUrl}inquiry`, data, headers)
                .then((response) => {
                    console.log(response);
                    button.text = "Success Inquiry";
                    // success go to video chat view
                    button.page.frame.navigate("videochat/videochat-page");
                })
                .catch((error) => {
                    console.log(error);
                    button.text = "Error inquiry";
                });
        },

        setAccessToken(accessToken) {
            this.accessToken = accessToken;
        },

        getAccessToken() {
            return this.accessToken;
        }
    });

    return viewModel;
}

module.exports = HomeViewModel;
