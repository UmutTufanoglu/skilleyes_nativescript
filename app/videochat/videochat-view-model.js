const observableModule = require("tns-core-modules/data/observable");

const SelectedPageService = require("../shared/selected-page-service");

const topmost = require('tns-core-modules/ui/frame');
const fromAsset = require('tns-core-modules/image-source');
const CameraPlus = require('@nstudio/nativescript-camera-plus');

const webViewModule = require("tns-core-modules/ui/web-view");

const camera = require("nativescript-camera");
const dialogs = require("tns-core-modules/ui/dialogs");

// import { Pubnub, PubnubConfig, PubnubOptions } from 'ns-pubnub';

// configure
const pubnubConfig = {
    publishKey: 'pub-c-aef19144-b0cb-4a9e-8e38-bdb3f14ee7b5',
    subscribeKey: 'sub-c-e036c610-00f0-11e8-8274-06cc4b902b08'
};

function VideoChatViewModel() {
    SelectedPageService.getInstance().updateSelectedPage("Videochat");

    camera.requestPermissions().then(
        function success() {
            dialogs.alert({
                title: "Berechtigung erfolgreich gegeben!",
                okButtonText: "Ok"
            }).then(function () {
                console.log("Dialog closed!");
            });
        },
        function failure() {
            dialogs.alert({
                title: "Berechtigung nicht gegeben!",
                okButtonText: "Ok"
            }).then(function () {
                console.log("Dialog closed!");
            });
        }
    );

    const viewModel = observableModule.fromObject({

    });

    return viewModel;
}

module.exports = VideoChatViewModel;
