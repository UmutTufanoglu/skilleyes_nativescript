const app = require("tns-core-modules/application");

const VideoChatViewModel = require("./videochat-view-model");

function onNavigatingTo(args) {
    const page = args.object;
    page.bindingContext = new VideoChatViewModel();
}

function onDrawerButtonTap(args) {
    const sideDrawer = app.getRootView();
    sideDrawer.showDrawer();
}

exports.onNavigatingTo = onNavigatingTo;
exports.onDrawerButtonTap = onDrawerButtonTap;
