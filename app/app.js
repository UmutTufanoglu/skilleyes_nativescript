/*
In NativeScript, the app.js file is the entry point to your application.
You can use this file to perform app-level initialization, but the primary
purpose of the file is to pass control to the app’s first module.
*/
require("./bundle-config");
const application = require("tns-core-modules/application");
const firebase = require("nativescript-plugin-firebase");

firebase.init({
    // Optionally pass in properties for database, authentication and cloud messaging,
    // see their respective docs.
}).then(
    () => {
        console.log("firebase.init done");
    },
    error => {
        console.log(`firebase.init error: ${error}`);
    }
);

firebase.addOnPushTokenReceivedCallback(
    (token) => {
        console.log("Firebase push token: " + token);
    }
);

firebase.addOnMessageReceivedCallback(
    (message) => {
        // save client_id + stream_id + avatar_url
        globel.data = message.data;
        global.client_id = message.data.client_id;
        global.stream_id = message.data.stream_id;
        global.avatar_url = message.data.avatar_url;

        console.log("client_id: " + global.client_id);
        console.log("stream_id: " + global.stream_id);
        console.log("avatar_url: " + global.avatar_url);
    }
);

firebase.getCurrentPushToken().then((token) => {
    // may be null if not known yet
    if (token != null) global.registrationToken = token;
    console.log(`Current push token: ${token}`);
});

application.run({ moduleName: "app-root/app-root" });

/*
Do not place any code after the application has been started as it will not
be executed on iOS.
*/
