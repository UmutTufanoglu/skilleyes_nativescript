var urlargs = urlparams();

var pkey = document.getElementById("pub_key");
var skey = document.getElementById("sub_key");

var keyBtn = document.getElementById("create-connection-btn");
var keyText = document.getElementById("create-connection-text");
var skilleyesFloatingAction = document.getElementById('skilleyes-logo-floating-action-centered');
var streamLoadingElement = document.getElementById('stream-is-loading');
var video_out = document.getElementById("vid-box");
var userId = "";
var standby_suffix = "-stdby";

if ('pub_key' in urlargs) {
    pkey.value = urlargs.pub_key;
}

if ('sub_key' in urlargs) {
    skey.value = urlargs.sub_key;
}

urlargs.pub_key = pkey.value;
urlargs.sub_key = skey.value;

function urlparams() {
    var params = {};
    if (location.href.indexOf('?') < 0) return params;

    PUBNUB.each(
        location.href.split('?')[1].split('&'),
        function (data) {
            var d =
                data.split('='); params[d[0]] = d[1];
        }
    );

    return params;
}

function urlstring(params) {
    return location.href.split('?')[0] + '?' + PUBNUB.map(
        params, function (key, val) { return key + '=' + val }
    ).join('&');
}

(function login(form) {
    // userId = form.username.value || "Anonymous";
    userId = document.getElementById('username').value;
    var userIdStdBy = userId + standby_suffix;
    var pubnub = window.pubnub = PUBNUB({
        publish_key: pkey.value,
        subscribe_key: skey.value,
        uuid: userId
    });

    pubnub.subscribe({
        channel: userIdStdBy,
        message: incomingCall,
        connect: function (e) {
            pubnub.state({
                channel: userIdStdBy,
                uuid: userId,
                state: { "status": "Available" },
                callback: function (m) {
                    // do some styling stuff if success
                    // form.login_submit.disabled = true;
                    // form.username.style.background = "#55ff5b";
                }
            });
            console.log("Subscribed and ready!");
        }
    });
    return false;
}());

function loginAgain(form) {
    // userId = form.username.value || "Anonymous";
    userId = document.getElementById('username').value;
    var userIdStdBy = userId + standby_suffix;
    var pubnub = window.pubnub = PUBNUB({
        publish_key: pkey.value,
        subscribe_key: skey.value,
        uuid: userId
    });

    pubnub.subscribe({
        channel: userIdStdBy,
        message: incomingCall,
        connect: function (e) {
            pubnub.state({
                channel: userIdStdBy,
                uuid: userId,
                state: { "status": "Available" },
                callback: function (m) {
                    // do some styling stuff if success
                    // form.login_submit.disabled = true;
                    // form.username.style.background = "#55ff5b";
                }
            });
            console.log("Subscribed and ready!");
        }
    });
    return false;
}

function incomingCall(m) {
    console.log("Connecting ...");
    video_out.innerHTML = "Connecting...";
    setTimeout(function () {
        // if (!window.phone) phoneStart();
        phoneStart(); // TODO
        phone.dial(m["call_user"]);
    }, 2000);
}

function showStreamLoading () {
    skilleyesFloatingAction.style.display = 'none';
    streamLoadingElement.style.display = 'block';
}

function hideStreamLoading () {
    skilleyesFloatingAction.style.display = 'block';
    streamLoadingElement.style.display = 'none';
}

function hideStreamLoader () {
    streamLoadingElement.style.display = 'block';
}

function hideStreamLogo () {
    streamLoadingElement.style.display = 'none';
}

//jsonCall.put(Constants.JSON_CALL_USER, username);
//jsonCall.put(Constants.JSON_CALL_TIME, System.currentTimeMillis());
function makeCall(form) {
    if (!window.pubnub) alert("Login First!");
    // Publish to their standby.

    showStreamLoading();

    var callUser = document.getElementById('user-dial-number').value;
    //var callUser = form.number.value;
    var stdByCh = callUser + standby_suffix;
    var msg = { "call_user": userId, "call_time": new Date().getMilliseconds() };
    window.pubnub.publish({
        channel: stdByCh,
        message: msg,
        callback: function (m) { console.log(m); }
    });
    if (!window.phone) phoneStart();
    // window.phone.dial(callUser);
    return false;
}

// TODO if the user is not an admin and tries to start the stream (watch) call this function
function watchCall(form) {
    console.log("inside watch call function");
    if (!window.phone) {
        watchStart();
    }
    watchStart();
    return false;
}

function watchStart() {
    var phone = window.phone = PHONE({
        // ssl: true,
        number: userId || "Anonymous",
        publish_key: pkey.value,
        subscribe_key: skey.value,
        media: {
            // TODO Kein Zwang bezüglich Mikrofon aber eine INFO diesbezüglich erstellen
            audio: false, // turn off mic
            video: false // turn off camera
        }
    });

    // Start the stream even if there is no microphone active
    phone.ready(function () {
        console.log("Phone ON!");
    });

    phone.receive(function (session) {
        session.message(message);
        session.connected(function (session) {
            video_out.innerHTML = "";
            video_out.appendChild(session.video);
            session.video.className = "streamVideo";// give session.video a class name
            video_out.style.css.width = '1280px';
            video_out.style.css.height = '720px';
            hideStreamLoader();
            hideStreamLogo();
        });
        session.ended(function (session) {
            video_out.innerHTML = '';
            hideStreamLoading();
        });
    });
}

function phoneStart() {
    // var phone = window.phone = PHONE({
    //     number: userId || "Anonymous", // listen on username line else Anonymous
    //     publish_key: pkey.value,     // Your Pub Key
    //     subscribe_key: skey.value, // Your Sub Key
    // });

    var phone = window.phone = PHONE({
        // ssl: true,
        number: userId || "Anonymous",
        publish_key: pkey.value,
        subscribe_key: skey.value,
        media: {
            audio : true,
            video : false /*turn off camera*/
        }
    });

    phone.ready(function () {
        console.log("Phone ON!");
    });
    phone.receive(function (session) {
        session.message(message);
        session.connected(function (session) {
            video_out.innerHTML = "";
            video_out.appendChild(session.video);
            session.video.className = "streamVideo";// give session.video a class name
        });
        session.ended(function (session) { video_out.innerHTML = ''; });
    });
}

function end() {
    if (window.phone) {
        window.phone.hangup();
    }
    // test
    // window.phone.hangup();
    hideStreamLoading();
}

function endAndSubmit() {
    end();
    document.getElementById('finish-stream-call-style').submit();
}

// TODO just close the video obj do not end the call since this method is called by a regular user not an admin
function closeVideo() {
    console.log("Test Video closed by a regular user, call is not ended on this function");
    video_out.innerHTML = '';
}

// Will format in 12-hour h:mm.s a time format
function formatTime(millis) {
    var d = new Date(millis);
    var h = d.getHours();
    var m = d.getMinutes();
    var s = d.getSeconds();
    var a = (Math.floor(h / 12) === 0) ? "am" : "pm";
    return (h % 12) + ":" + m + "." + s + " " + a;
}

// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=->
// XSS Prevent
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=->
function safetxt(text) {
    return ('' + text).replace(/[<>]/g, '');
}

function message(session, message) {
    add_chat(session.number, message);
}

// (function (i, s, o, g, r, a, m) {
// i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
//     (i[r].q = i[r].q || []).push(arguments)
// }, i[r].l = 1 * new
//     Date(); a = s.createElement(o),
//         m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
// })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

// ga('create', 'UA-46933211-3', 'auto');
// ga('send', 'pageview');